<?php

namespace Drupal\uw_news_common\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreateUsersForm for creating users.
 *
 * @package Drupal\uw_news_common\Form
 */
class CreateUsersForm extends FormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new AjaxFormBlock.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(MessengerInterface $messenger, LanguageManagerInterface $language_manager) {
    $this->messenger = $messenger;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('language_manager')
    );
  }

  /**
   * Creates a set number of users.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'create_users_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // The form element for the number of users to create.
    $num_users = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of users'),
      '#description' => $this->t('Enter the number of users to create, between 1 and 1000.'),
      '#required' => TRUE,
    ];

    // The submit button for the form.
    $submit = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    // The actions for the form.
    $actions = [
      '#type' => 'actions',
      'submit' => $submit,
    ];

    // The completed form.
    $form = [
      'num_user' => $num_users,
      'actions' => $actions,
    ];

    // Return the form elements.
    return $form;
  }

  /**
   * Validate the number of users for the form.
   *
   * @param array $form
   *   The array of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The array of the form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Get the number of users from the form state.
    $num_users = $form_state->getValue('num_user');

    // If the number of users is not between 1 and 1000, set an error.
    if ($num_users < 1 || $num_users > 1000) {

      // Set an error for number of users.
      $form_state->setErrorByName('num_users', $this->t('The number of users must be between 1 and 1000.'));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get the number of users from the form state.
    $num_users = $form_state->getValue('num_user');

    // Get the language for the current site.
    $language = $this->languageManager->getCurrentLanguage()->getId();

    $batch = $this->generateBatch($num_users, $language);

    batch_set($batch);
  }

  /**
   * Batch processing for creating users.
   *
   * @param int $num_users
   *   The num of users to create.
   * @param string $language
   *   The language for the user.
   *
   * @return array
   *   The array used for the batch.
   */
  public function generateBatch(int $num_users, string $language): array {

    // The array used for the operations of the batch.
    $operations = [];

    // Get the modulus for the number of iterations in a batch.
    $mod = (int) $num_users / 10;

    // Step through and set an individual batch.
    for ($i = 0; $i < $mod; $i++) {

      // Each operation is an array consisting of
      // - The function to call.
      // - An array of arguments to that function.
      $op_details = [
        '_uw_news_common_batch_create_users',
        [
          $i + 1,
          $this->t('(Operation @operation)', ['@operation' => $i]),
          $language,
        ],
      ];

      $operations[] = $op_details;
    }

    // Setup the individual batch.
    $batch = [
      'title' => $this->t('Creating @num users', ['@num' => $num_users]),
      'operations' => $operations,
      'finished' => '_uw_news_common_batch_create_users_finished',
    ];

    // Return the batch.
    return $batch;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {

    // Get the roles of the current user.
    $roles = $account->getRoles();

    // Ensure that the current user is either user1 or has an
    // administrator role.
    return AccessResult::allowedIf($account->id() == 1 || in_array('administrator', $roles));
  }

}
